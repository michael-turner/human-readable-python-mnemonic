#!/usr/bin/env python
#
# Copyright (c) 2013 Pavol Rusnak
# Copyright (c) 2017 mruddy
# Copyright (c) 2018 bill-walker
# Copyright (c) 2018 michael-turner
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import binascii
import bisect
import hashlib
import hmac
import itertools
import os
import sys
import unicodedata


class ConfigurationError(Exception):
    pass


class Mnemonic(object):
    def __init__(self, language):
        self.radix = 1626
        with open('%s/%s.txt' % (self._get_directory(), language), 'r') as f:
            self.wordlist = [w.strip().decode('utf8') if sys.version < '3' else w.strip() for w in f.readlines()]
        if len(self.wordlist) != self.radix:
            raise ConfigurationError('Wordlist should contain %d words, but it contains %d words.' % (self.radix, len(self.wordlist)))
            print ("incorrect number of words")
        with open('%s/%s-3letters.txt' % (self._get_directory(), language), 'r') as f:
            self.wordlist3 = [w.strip().decode('utf8') if sys.version < '3' else w.strip() for w in f.readlines()]
        if len(self.wordlist3) != self.radix:
            raise ConfigurationError('Wordlist should contain %d words, but it contains %d words.' % (self.radix, len(self.wordlist3)))
            print ("incorrect number of words")

    @classmethod
    def _get_directory(cls):
        return os.path.join(os.path.dirname(__file__), 'wordlist-monero')

    @classmethod
    def list_languages(cls):
        return [f.split('.')[0] for f in os.listdir(cls._get_directory()) if f.endswith('.txt')]

    @classmethod
    def normalize_string(cls, txt):
        if isinstance(txt, str if sys.version < '3' else bytes):
            utxt = txt.decode('utf8')
        elif isinstance(txt, unicode if sys.version < '3' else str):  # noqa: F821
            utxt = txt
        else:
            raise TypeError("String value expected")

        return unicodedata.normalize('NFKD', utxt)

    @classmethod
    def detect_language(cls, code):
        code = cls.normalize_string(code)
        first = code.split(' ')[0]
        languages = cls.list_languages()

        for lang in languages:
            mnemo = cls(lang)
            if first in mnemo.wordlist:
                return lang

        raise ConfigurationError("Language not detected")

    # Adapted from <http://tinyurl.com/oxmn476>
    def to_entropy(self, words):
        if not isinstance(words, list):
            words = words.split(' ')
        if len(words) not in [13, 16, 19, 22, 25]:
            #raise ValueError('Number of words must be one of the following: [12, 15, 18, 21, 24], but it is not (%d).' % len(words))
            print ("incorrect number of words")
        # Look up all the words in the list and construct the
        # concatenation of the original entropy and the checksum.
        concatLenBits = len(words) * 11
        concatBits = [False] * concatLenBits
        wordindex = 0
        for word0, word1, word2 in zip(*[words[i::3]
            for i in range(3)]):
                # Find the words index in the wordlist
                ndx = 0 + self.wordlist3.index(word2[:3])
                ndx = ndx + 1626 * self.wordlist3.index(word1[:3])
                ndx = ndx + 1626 * 1626 * self.wordlist3.index(word0[:3])
                if ndx < 0:
                    raise LookupError('Unable to find "%s" in word list.' % word)
                # Set the next 11 bits to the value of the index.
                for ii in range(32):
                    concatBits[(wordindex * 32) + ii] = (ndx & (1 << (31 - ii))) != 0
                wordindex += 1
        # Extract original entropy as bytes.
        entropy = bytearray( ( len(words) - 1 ) // 3 * 4)
        for ii in range(len(entropy)):
            for jj in range(8):
                if concatBits[(ii * 8) + jj]:
                    entropy[ii] |= 1 << (7 - jj)
        # Take the digest of the entropy.
        b = hashlib.sha256(entropy).hexdigest()
        ndx=0
        for word in words:
            ndx=self.wordlist3.index(word[:3])
        if (int(b,16) % 1626) != ndx:
            #raise ValueError('Failed checksum.')
            print ("failed checksum")
        return entropy

    def to_mnemonic(self, data):
        if len(data) not in [16, 20, 24, 28, 32]:
            #raise ValueError('Data length should be one of the following: [16, 20, 24, 28, 32], but it is not (%d).' % len(data))
            print ("incorrect number of bytes")
        h = hashlib.sha256(data).hexdigest()
        b = bin(int(binascii.hexlify(data), 16))[2:].zfill(len(data) * 8) # + \
            #bin(int(h, 16))[2:].zfill(256)[:len(data) * 8 // 32]
        result = []
        for i in range(len(b) // 32):
            idx = int(b[i * 32:(i + 1) * 32], 2)
            a1=idx%1626
            a2=(idx//1626)%1626
            a3=(idx//(1626*1626))%1626
            result.append(self.wordlist[a3])
            result.append(self.wordlist[a2])
            result.append(self.wordlist[a1])
        result.append(self.wordlist[int(h,16)%1626])
        if self.detect_language(' '.join(result)) == 'japanese':  # Japanese must be joined by ideographic space.
            result_phrase = u'\u3000'.join(result)
        else:
            result_phrase = ' '.join(result)
        return result_phrase

    def expand_word(self, prefix):
        if prefix in self.wordlist:
            return prefix
        else:
            matches = [word for word in self.wordlist if word.startswith(prefix)]
            if len(matches) == 1:  # matched exactly one word in the wordlist
                return matches[0]
            else:
                # exact match not found.
                # this is not a validation routine, just return the input
                return prefix

    def expand(self, mnemonic):
        return ' '.join(map(self.expand_word, mnemonic.split(' ')))


def main():
    import binascii
    import sys
    if len(sys.argv) > 1:
        print"Do not enter secrets as command line arguments: they are stored in your logs. Make a new seed by running the program and typing, or pasting your words, then hit enter"
    else:
        data = sys.stdin.readline().strip()
        m = Mnemonic('english')
        print""

        if ' ' in data:
            print(binascii.hexlify(m.to_entropy(data)))
            if binascii.hexlify(m.to_entropy(data)) != binascii.hexlify(m.to_entropy(m.to_mnemonic(binascii.unhexlify(binascii.hexlify(m.to_entropy(data)))))):
                print""
                print"Self-test failed, do not use"
        else:
            data = binascii.unhexlify(data)
            print(m.to_mnemonic(data))
            if m.to_mnemonic(data) != m.to_mnemonic(binascii.unhexlify(binascii.hexlify(m.to_entropy(m.to_mnemonic(data))))):
                print""
                print"Self-test failed, do not use"
        print""


if __name__ == '__main__':
    main()


